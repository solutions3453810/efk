# Стек EFK 

*********************************************************************************************
## Задание

### Что нужно сделать

Разверните у себя стек ELK/EFK или Graylog.
Придумайте минимум три таргета для сбора логов (чем больше, тем лучше для вас, это может быть тот же Rsyslog и nginx.log). Произвольно выберите два таргет-файла, один таргет найдите сами, но такой, который можете инициировать руками — как в видео логин-логаут из-под рута, но он уже был, поэтому не принимается)
Заведите таргеты.
Прикрутите alerts или выполнение кастомных скриптов при срабатывании какого-либо события. Каналы уведомлений можно не настраивать, достаточно сконфигурированного и отработавшего алерта или ивента.
Инициируйте таргет и зафиксируйте его скриншотом, алертом или ивентом.


### Рекомендации

Если вы не знаете, что можете трекать, напишите утилиту на Bash или Python, которая будет в фоне писать что-то наподобие lorem ipsum в вымышленный лог-файл.
Под таргетом во втором случае подразумевается сообщение о каком-то событии в лог-файле. Подумайте, что у вас происходит в системе и как вы можете на это влиять. 

*********************************************************************************************

## Решение

Разворачивается из docker compose (elasticsearch из одной ноды) 

Сделано по следующей схеме
fluentd (client) --> fluentd (collector) --> elasticsearch <--kibana + curator

Вместо curator имеет смысл использовать index lifecycle policy.

fluentd считывает логи контейнера nginx (stdout, stderr), auth.log, bash history на клиенте и отправляет их на fluentd сервера. Который их сортирует по индексам и отправляет в elasticsearch.

Алертинг kibana (rules and connectors) требует зашифрованного соединения между elasticsearch и kibana, поэтому созданы сертификаты и подключены к контейнерам в docker-compose. Инструкция по созданию - cert_create.txt.
