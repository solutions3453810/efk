#!/bin/sh

MIN=35

cat /curator/curator.yml | \
sed "s/CURATOR_USERNAME/${CURATOR_USERNAME}/g" | \
sed "s/CURATOR_PASSWORD/${CURATOR_PASSWORD}/g" > /tmp/curator.yml

mv /tmp/curator.yml /curator/curator.yml

echo -e "${MIN} 23 * * * /usr/bin/curator --config /curator/curator.yml /curator/action.yml" > /etc/crontabs/root


exec "$@"